<?php
if($_SERVER['REQUEST_METHOD'] == 'POST') {
    //validation
    $id = $_POST['id'];
    $name = $_POST['name'];
    $group = $_POST['group'];
    $birthday = $_POST['birthday'];
    $gender = $_POST['gender'];
    
    $errors = array();
    $succes = array();
    if(empty($name)) {
      $errors['name'] = 'Name is required.';
    } else if(!preg_match('/^[a-zA-Z-]+$/', $name)) {
      $errors['name'] = 'Name can only contain letters.';
    } else {
      $succes['name'] = 'true';
    }
    if(empty($group)) {
      $errors['group'] = 'Group is required.';
    } else if(!preg_match('/^[a-zA-Z0-9-]+$/', $group)) {
      $errors['group'] = 'Group can only contain letters and numbers.';
    } else {
      $succes['group'] = 'true';
    }
    if(empty($birthday)) {
      $errors['birthday'] = 'Birthday is required.';
    } else {
      $birthday_datetime = new DateTime($_POST['birthday']);
      $now = new DateTime();
      $age = $now->diff($birthday_datetime)->y;
      if($age < 16) {
        $errors['birthday'] = 'Student must be 16 or older.';
      } else {
        $succes['birthday'] = 'true';
      }
    }
    if(empty($gender)) {
      $errors['gender'] = 'gender is required.';
    } else {
      $succes['gender'] = 'true';
    }

    //If not valid send arrors else add student
    if(count($errors) > 0) {
        $response = array('success' => false, 'errors' => $errors);
        header('Content-Type: application/json');
        echo json_encode($response);
    } else {
        $host = 'localhost';
        $username = 'root';
        $password = '';
        $database = 'students';
        try {
            $pdo = new PDO("mysql:host=$host;dbname=$database", $username, $password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (PDOException $e) {
            // Return an error response if the database connection fails
            header('HTTP/1.1 500 Internal Server Error');
            header('Content-Type: application/json; charset=UTF-8');
            die(json_encode(array('message' => 'Database connection failed')));
        }

        $sql = "UPDATE students SET group_name=:group_name, name=:name, gender=:gender, birthday=:birthday WHERE id=:id";
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['group_name' => $group, 'name' => $name, 'gender' => $gender, 'birthday' => $birthday, 'id' => $id]);

        $sql = "SELECT * FROM students";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $students = $stmt->fetchAll(PDO::FETCH_ASSOC);

        header('Content-Type: application/json');
        $response = array('success' => true, 'students' => $students);
        echo json_encode($response);
    }
}
?>