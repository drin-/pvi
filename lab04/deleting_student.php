<?php
if($_SERVER['REQUEST_METHOD'] == 'POST') {
$id = $_POST['id'];

// Connect to the database
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'students';
try {
    $pdo = new PDO("mysql:host=$host;dbname=$database", $username, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e) {
    // Return an error response if the database connection fails
    header('HTTP/1.1 500 Internal Server Error');
    header('Content-Type: application/json; charset=UTF-8');
    die(json_encode(array('message' => 'Database connection failed')));
}

// Delete the student from the database
$sql = "DELETE FROM students WHERE id=:id";
$stmt = $pdo->prepare($sql);
$stmt->execute(['id' => $id]);

// Adjust IDs for remaining students
try {
$stmt = $pdo->prepare("SET @count = 0");
$stmt->execute();
$stmt = $pdo->prepare("UPDATE students SET id = @count:= @count + 1");
$stmt->execute();
}
catch (PDOException $e) {
    // Return an error response if the database connection fails
    header('HTTP/1.1 500 Internal Server Error');
    header('Content-Type: application/json; charset=UTF-8');
    die(json_encode(array('message' => 'Database connection failed')));
}

try {
// Get all students from the database
$sql = "SELECT * FROM students ORDER BY id";
$stmt = $pdo->prepare($sql);
$stmt->execute();
$students = $stmt->fetchAll(PDO::FETCH_ASSOC);
}
catch (PDOException $e) {
    // Return an error response if the database connection fails
    header('HTTP/1.1 500 Internal Server Error');
    header('Content-Type: application/json; charset=UTF-8');
    die(json_encode(array('message' => 'Database connection failed')));
}


// Return the students as JSON
header('Content-Type: application/json');
$response = array('success' => true, 'students' => $students);
header('Content-Type: application/json');
echo json_encode($response);
}
?>