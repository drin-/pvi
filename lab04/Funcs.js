setTimeout(function(){loadStudentTable()}, 100);
const formAdd = document.getElementById("AddingModalContent");
let studentIds = [];
const modalAdd = document.getElementById("AddingModal");
const modalDel = document.getElementById("AreYouSureModal");
const modalNot = document.getElementById("NotificationModal");
const modalProf = document.getElementById("ProfileModal");
const btnAdd = document.getElementById("AddStudent");
const spanAdd = document.getElementsByClassName("close")[0];
const spanDel = document.getElementsByClassName("close")[1];
const NotBell = document.getElementById("Notification-Bell");
const ProfFot = document.getElementById("Avatar");
const NotCountIcon = document.getElementById("notificationCount");
const table = document.getElementById("student-table");

const hiddenID = document.getElementById("HiddenId");
const group = document.getElementById("group");
const name = document.getElementById("name");
const gender = document.getElementById("gender");
const birthday = document.getElementById("birthday");

// Initialize variables
let NotCount = 0;

// Event listeners
NotBell.addEventListener("dblclick", () => {
    NotCount++;
    NotCountIcon.innerHTML = String(NotCount);
    NotCountIcon.style.display = "block";
});

NotBell.addEventListener("mouseover", () => {
    modalNot.style.display = "block";
});

NotBell.addEventListener("mouseout", () => {
    modalNot.style.display = "none";
});

ProfFot.addEventListener("mouseover", () => {
    modalProf.style.display = "block";
});

ProfFot.addEventListener("mouseout", () => {
    modalProf.style.display = "none";
});

btnAdd.addEventListener("click", () => {
    openModal(false, null);
});

spanAdd.addEventListener("click", () => {
    modalAdd.style.display = "none";
    resetValueById("group");
    resetValueById("name");
    resetValueById("gender");
    resetValueById("birthday");

    setDefault(group);
    setDefault(name);
    setDefault(gender);
    setDefault(birthday);
});

spanDel.addEventListener("click", () => {
    modalDel.style.display = "none";
});

window.addEventListener("click", (event) => {
    if (event.target == modalAdd || event.target == modalDel) {
        modalAdd.style.display = "none";
        modalDel.style.display = "none";

        resetValueById("group");
        resetValueById("name");
        resetValueById("gender");
        resetValueById("birthday");
    
        setDefault(group);
        setDefault(name);
        setDefault(gender);
        setDefault(birthday);
    }
});

const setDefault = (element) => {
    const inputControl = element.parentElement;

    inputControl.classList.remove('success');
    inputControl.classList.remove('error');
}

const setError = (element) => {
    const inputControl = element.parentElement;
    inputControl.classList.add('error');
    inputControl.classList.remove('success');
}

const setSuccess = element => {
    const inputControl = element.parentElement;
    inputControl.classList.add('success');
    inputControl.classList.remove('error');
};

function createButton(text) {
    const button = document.createElement("button");
    button.textContent = text;
    return button;
}
  
function getValueById(id) {
    return document.getElementById(id).value;
}
  
function resetValueById(id) {
    document.getElementById(id).value = "";
}

function editButtonFunc(rowToEdit) {
    openModal(true, rowToEdit);
}
formAdd.addEventListener('submit', function(event) {
    event.preventDefault(); 
})
function updateStudentTable(data) {
    // Clear the table body
    table.empty();

    // Add each student to the table
    data.forEach(function(student) {
        var $row = $('<tr>');
        $row.append($('<td>').text(student.name));
        $row.append($('<td>').text(student.age));
        $row.append($('<td>').text(student.email));
        $tbody.append($row);
    });
}



// Load the student table when the page is first loaded
function openModal(isEditMode, itemToEdit) {
    const headingText = document.getElementById("AddingModalHeader");
    const addEditButt = document.getElementById("AddEditButt");
    editing = isEditMode;

    if (isEditMode) {
        addEditButt.value = "Confirm";
        headingText.textContent = "Editing a student";

        group.value = table.rows[itemToEdit].cells[1].innerHTML;
        name.value = table.rows[itemToEdit].cells[2].innerHTML;
        gender.value = table.rows[itemToEdit].cells[3].innerHTML;
        birthday.value = table.rows[itemToEdit].cells[4].innerHTML;


        modalAdd.style.display = "block";
        addEditButt.onclick = () => {
            edit(itemToEdit);
        };
    } else {
        addEditButt.value = "Add";
        headingText.textContent = "Adding a new student";
        modalAdd.style.display = "block";

        addEditButt.onclick = () => {
            addingNewStudent();
        };
    }
}

  

function addingNewStudent() {
    $.ajax({
        type: 'POST',
        url: 'adding_student.php',
        dataType: 'json',
        data: {name: name.value, group: group.value, birthday: birthday.value, gender: gender.value},
        success: function(data) {
            var response = data;
            if(response.success) {
                updateStudentTable(response.students);
                modalAdd.style.display = "none";
            } else {
                var errors = response.errors;
                var errorMessages = '';
                for(var key in errors) {
                    setError(document.getElementById(key));
                    errorMessages += errors[key] + '\n';
                }
                alert(errorMessages);
            }
        },
        error: function(xhr, status, error) {
            console.log(xhr.responseText);
        }
    });
}
function edit(itemToEdit) {
    
    $.ajax({
        type: 'POST',
        url: 'editing_student.php',
        dataType: 'json',
        data: {name: name.value, id: itemToEdit, group: group.value, birthday: birthday.value, status: status.value, gender: gender.value},
        success: function(data) {
            var response = data;
            if(response.success) {
                updateStudentTable(response.students);
                modalAdd.style.display = "none";
            } else {
                var errors = response.errors;
                var errorMessages = '';
                for(var key in errors) {
                    setError(document.getElementById(key));
                    errorMessages += errors[key] + '\n';
                }
                alert(errorMessages);
            }
        },
        error: function(xhr, status, error) {
            console.log(xhr.responseText);
        }
    });
}

function deleteButtonFunc(rowToDelete) {
    modalDel.style.display = "block";
    var yesBut = document.getElementById("yesBut");
    var noBut = document.getElementById("noBut");
    yesBut.onclick = () => {
        
        // Send a POST request to the delete_student.php script
        $.ajax({
          type: 'POST',
          url: 'deleting_student.php',
          data: {id: rowToDelete},
          dataType: 'json',
          success: function(data) {
            var response = data;
            if(response.success) {
                updateStudentTable(response.students);
                modalDel.style.display = "none";
            } else {
                console.log(data);
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log('Error deleting student: ' + textStatus + ' - ' + errorThrown);
          }
        });
    }
    noBut.onclick = () => {
        modalDel.style.display = "none";
    }
}
// Function to load the student table from the database
function loadStudentTable() {
    // Send a GET request to PHP to get the student table
    $.ajax({
        type: 'GET',
        url: 'get_students.php',
        dataType: 'json',
        success: function(data) {
            // Update the student table with the data
            updateStudentTable(data);
        },
        error: function(xhr, status, error) {
            console.log(xhr.responseText);
        }
    });
}
// Function to update the student table with new data
function updateStudentTable(data) {
    // Clear the table body
    // Add each student to the table
    $('#student-table tr:not(:first)').remove();

    data.forEach(function(student) {
        const checkboxListener = document.createElement("input");
        checkboxListener.type = "checkbox";
        const editButton = createButton("EDIT");
        const deleteButton = createButton("DELETE");
        const newRow = table.insertRow(-1);
        const checkBoxCell = newRow.insertCell(0);
        const groupCell = newRow.insertCell(1);
        const nameCell = newRow.insertCell(2);
        const genderCell = newRow.insertCell(3);
        const birthdayCell = newRow.insertCell(4);
        const statusCell = newRow.insertCell(5);
        const actionsCell = newRow.insertCell(6);

        checkBoxCell.appendChild(checkboxListener);
        groupCell.innerHTML = student.group_name;
        nameCell.innerHTML = student.name;
        genderCell.innerHTML = student.gender;
        birthdayCell.innerHTML = student.birthday;
        statusCell.innerHTML = "online";
        actionsCell.appendChild(editButton);
        actionsCell.appendChild(deleteButton);

        resetValueById("group");
        resetValueById("name");
        resetValueById("gender");
        resetValueById("birthday");
        
        deleteButton.onclick = () => {
            deleteButtonFunc(deleteButton.parentNode.parentNode.rowIndex);
        };
        editButton.onclick = () => {
            editButtonFunc(editButton.parentNode.parentNode.rowIndex);
        };
    });
}