<?php
// Connect to the database
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'students';
$pdo = new PDO("mysql:host=$host;dbname=$database", $username, $password);

// Get all students from the database
$sql = "SELECT * FROM students";
$stmt = $pdo->prepare($sql);
$stmt->execute();
$students = $stmt->fetchAll(PDO::FETCH_ASSOC);

// Return the students as JSON
header('Content-Type: application/json');
echo json_encode($students);
?>