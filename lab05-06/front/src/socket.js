import { reactive } from "vue";
import { io } from "socket.io-client";

export const state = reactive({
  connected: false,
});

export const createSocket = (url, userId) => {
  const socket = io(url, {
    query: {
      userId: userId,
    },
    forceNew: true,
    reconnectionAttempts: "infinity",
    timeout: 10000,
    transports: ["websocket"]
  });

  socket.on("connect", () => {
    state.connected = true;
  });

  socket.on("disconnect", () => {
    state.connected = false;
  });

  return socket;
};