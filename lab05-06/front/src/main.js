import App from './App.vue'
import { createApp } from 'vue'
import { createRouter, createWebHashHistory } from 'vue-router'

import store from './store'

// import EmptyView from '@/./components/EmptyView.vue';
import SignIn from '@/./components/Auth/SignIn.vue';
import SignUp from '@/./components/Auth/SignUp.vue';
import StudentsChat from '@/./components/StudentsChat.vue';

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faPlus, faPencil, faXmark, faUser as faSolidUser, faPaperPlane } from '@fortawesome/free-solid-svg-icons'
import { faUser, faBell } from '@fortawesome/free-regular-svg-icons'
library.add(faPlus, faUser, faPencil, faXmark, faBell, faSolidUser, faPaperPlane);

const routes = [
    { path: '/sign-in', component: SignIn },
    { path: '/sign-up', component: SignUp },
    { path: '/chat', component: StudentsChat },
];

const router = createRouter({
    history: createWebHashHistory(),
    routes,
});

createApp(App)
    .component('font-awesome-icon', FontAwesomeIcon)
    .use(router)
    .use(store)
    .mount('#app');
