const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const http = require('http');
const socketio = require('socket.io');
const User = require('./models/user');
const Message = require('./models/message');

const connectionString = 'mongodb+srv://andriisymonhermanpz2021:tadZjZLKUOr4tGk4@pvi.c7nverc.mongodb.net/?retryWrites=true&w=majority';
mongoose.connect(connectionString, {
    dbName: 'StudentDB',
    useNewUrlParser: true,
    useUnifiedTopology: true,
})

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("StudentDB connected");

    const app = express();
    const httpsPort = 3000;
    const httpPort = 3080;
    const router = require('./router')

    app.use(cors());
    app.use(express.json());
    app.use('/', router);

    const server = http.createServer(app);
    CreateSocket(server);

    app.listen(httpsPort, () => console.log(`Server started on httpsPort ${httpsPort}`));
    server.listen(httpPort, () => console.log(`Socket started on httpsPort ${httpPort}`));
});

function CreateSocket(port) {
  const io = socketio(port, {
    cors: {
      origin: "*",
    }
  });

  io.on('connection', async (socket) => {
    console.log('User connected:', socket.id);

    const userId = socket.handshake.query.userId;

    const fromUser = await User.findById(userId);

    if (!fromUser) {
      console.log('User not found');
      socket.disconnect();
      return;
    }

    socket.join(userId);
    console.log('User join success. Id: ' + fromUser._id);

    socket.on('message', async (message) => {
      const { fromId, toId, content } = message;


      const toUser = await User.findById(toId);

      if (!toUser) {
        console.log('User not found');
        return;
      }

      const newMessage = new Message({
        fromId,
        toId,
        content,
      });

      await newMessage.save();

      var newMessageData = {
        fromId: newMessage.fromId,
        toId: newMessage.toId,
        fromLogin: fromUser.login,
        toLogin: toUser.login,
        content: newMessage.content
      };

      io.to(fromId).emit('message', newMessageData);
      io.to(toId).emit('message', newMessageData);
    });

    socket.on('disconnect', () => {
      console.log('User disconnected:', socket.id);
    });
  });
};