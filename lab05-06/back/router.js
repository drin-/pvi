const express = require('express');
const router = express.Router();
const messageController = require('./controllers/messageController');
const userController = require('./controllers/userController');

router
    .route('/mess')
    .get(messageController.getMessagesByIds)

router
    .route('/unread')
    .get(messageController.getUnreadMessagesByReceiverId);
router
    .route('/sign-up')
    .post(userController.signUpUser);
router
    .route('/sign-in')
    .post(userController.signInUser);

router
    .route('/user/all')
    .get(userController.getAllUsers);

module.exports = router;