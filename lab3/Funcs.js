// Get references to HTML elements
const formAdd = document.getElementById("AddingModalContent")

const modalAdd = document.getElementById("AddingModal");
const modalDel = document.getElementById("AreYouSureModal");
const modalNot = document.getElementById("NotificationModal");
const modalProf = document.getElementById("ProfileModal");
const btnAdd = document.getElementById("AddStudent");
const spanAdd = document.getElementsByClassName("close")[0];
const spanDel = document.getElementsByClassName("close")[1];
const NotBell = document.getElementById("Notification-Bell");
const ProfFot = document.getElementById("Avatar");
const NotCountIcon = document.getElementById("notificationCount");
const table = document.getElementById("student-table");

const hiddenID = document.getElementById("HiddenId");
const group = document.getElementById("group");
const name = document.getElementById("name");
const gender = document.getElementById("gender");
const birthday = document.getElementById("birthday");
const status = document.getElementById("status");

// Initialize variables
let NotCount = 0;

// Event listeners
NotBell.addEventListener("dblclick", () => {
    NotCount++;
    NotCountIcon.innerHTML = String(NotCount);
    NotCountIcon.style.display = "block";
});

NotBell.addEventListener("mouseover", () => {
    modalNot.style.display = "block";
});

NotBell.addEventListener("mouseout", () => {
    modalNot.style.display = "none";
});

ProfFot.addEventListener("mouseover", () => {
    modalProf.style.display = "block";
});

ProfFot.addEventListener("mouseout", () => {
    modalProf.style.display = "none";
});

btnAdd.addEventListener("click", () => {
    openModal(false, null);
});

spanAdd.addEventListener("click", () => {
    modalAdd.style.display = "none";
    resetValueById("group");
    resetValueById("name");
    resetValueById("gender");
    resetValueById("birthday");
    resetValueById("status");

    setDefault(group);
    setDefault(name);
    setDefault(gender);
    setDefault(birthday);
    setDefault(status);
});

spanDel.addEventListener("click", () => {
    modalDel.style.display = "none";
});

window.addEventListener("click", (event) => {
    if (event.target == modalAdd || event.target == modalDel) {
        modalAdd.style.display = "none";
        modalDel.style.display = "none";

        resetValueById("group");
        resetValueById("name");
        resetValueById("gender");
        resetValueById("birthday");
        resetValueById("status");
    
        setDefault(group);
        setDefault(name);
        setDefault(gender);
        setDefault(birthday);
        setDefault(status);
    }
});
//Validation
const setDefault = (element) => {
    const inputControl = element.parentElement;

    inputControl.classList.remove('success');
    inputControl.classList.remove('error');
}

const setError = (element, message) => {
    const inputControl = element.parentElement;
    inputControl.classList.add('error');
    inputControl.classList.remove('success');
    window.alert(message);
}

const setSuccess = element => {
    const inputControl = element.parentElement;
    inputControl.classList.add('success');
    inputControl.classList.remove('error');
};
const onlyLettersAndNumbers = (str) => {
    return /^[A-Za-z0-9-]*$/.test(str);
}
const onlyLetters = (str) => {
    return /^[A-Za-z-]*$/.test(str);
}
const validateInputs = () => {
    const groupVal = group.value.trim();
    const nameVal = name.value.trim();
    const genderVal = gender.value;
    const birthdayVal = birthday.value;
    const statusVal = status.value;
    var isValid = true;

    if(groupVal === '') {
        setError(group, 'Group is required');
        isValid = false;
    } else if(!onlyLettersAndNumbers(groupVal)){
        setError(group, 'Group can not contain any special characters!');
        isValid = false;
    } else {
        setSuccess(group);
    }

    if(nameVal === '') {
        setError(name, 'Name is required');
        isValid = false;
    } else if(!onlyLetters(nameVal)){
        setError(name, 'Name can nonly contain letters!');
        isValid = false;
    } else {
        setSuccess(name);
    }

    if(genderVal === '') {
        setError(gender, 'Gender is required');
        isValid = false;
    } else {
        setSuccess(gender);
    }

    if(birthdayVal === '') {
        setError(birthday, 'Birthday is required');
        isValid = false;
    } else {
        setSuccess(birthday);
    }

    if(statusVal === '') {
        setError(status, 'Status is required');
        isValid = false;
    } else {
        setSuccess(status);
    }
    if(isValid){
        setDefault(group);
        setDefault(name);
        setDefault(gender);
        setDefault(birthday);
        setDefault(status);
    }

    return isValid;
};
//Functions
function openModal(isEditMode, itemToEdit) {
    const headingText = document.getElementById("AddingModalHeader");
    const addEditButt = document.getElementById("AddEditButt");

    if (isEditMode) {
        addEditButt.value = "Confirm";
        headingText.textContent = "Editing a student";

        group.value = table.rows[itemToEdit].cells[1].innerHTML;
        name.value = table.rows[itemToEdit].cells[2].innerHTML;
        gender.value = table.rows[itemToEdit].cells[3].innerHTML;
        birthday.value = table.rows[itemToEdit].cells[4].innerHTML;
        status.value = table.rows[itemToEdit].cells[5].innerHTML;


        modalAdd.style.display = "block";
        addEditButt.onclick = () => {
            confirmEdit(itemToEdit);
        };
    } else {
        addEditButt.value = "Add";
        headingText.textContent = "Adding a new student";
        modalAdd.style.display = "block";

        addEditButt.onclick = () => {
            addingNewStudent();
        };
  }
}

function addingNewStudent() {
    event.preventDefault();
    if(!validateInputs())
        return;

    const checkboxListener = document.createElement("input");
    checkboxListener.type = "checkbox";
  
    const editButton = createButton("EDIT");
    const deleteButton = createButton("DELETE");

    const newRow = table.insertRow(-1);
    const checkBoxCell = newRow.insertCell(0);
    const groupCell = newRow.insertCell(1);
    const nameCell = newRow.insertCell(2);
    const genderCell = newRow.insertCell(3);
    const birthdayCell = newRow.insertCell(4);
    const statusCell = newRow.insertCell(5);
    const actionsCell = newRow.insertCell(6);

  
    checkBoxCell.appendChild(checkboxListener);
    groupCell.innerHTML = group.value;
    nameCell.innerHTML = name.value;
    genderCell.innerHTML = gender.value;
    birthdayCell.innerHTML = birthday.value;
    statusCell.innerHTML = status.value;
    hiddenID.value = table.rows.length - 1;

    actionsCell.appendChild(editButton);
    actionsCell.appendChild(deleteButton);

    modalAdd.style.display = "none";
  
    var addedStudentValues = {
        group: group.value,
        name: name.value,
        gender: gender.value,
        birthday: birthday.value,
        status: status.value,
        ID: hiddenID.value
    }
    console.log(addedStudentValues);
    
    resetValueById("group");
    resetValueById("name");
    resetValueById("gender");
    resetValueById("birthday");
    resetValueById("status");
  
    deleteButton.onclick = () => {
        deleteButtonFunc(deleteButton.parentNode.parentNode.rowIndex);
    };
    editButton.onclick = () => {
        editButtonFunc(editButton.parentNode.parentNode.rowIndex);
    };
}
  
function createButton(text) {
    const button = document.createElement("button");
    button.textContent = text;
    return button;
}
  
function getValueById(id) {
    return document.getElementById(id).value;
}
  
function resetValueById(id) {
    document.getElementById(id).value = "";
}

function deleteButtonFunc(rowToDelete) {
    modalDel.style.display = "block";
    var yesBut = document.getElementById("yesBut");
    var noBut = document.getElementById("noBut");
    yesBut.onclick = () => {
        table.deleteRow(rowToDelete);
        modalDel.style.display = "none";
    }
    noBut.onclick = () => {
        modalDel.style.display = "none";
    }
}

function editButtonFunc(rowToEdit) {
    openModal(true, rowToEdit);
}

function confirmEdit(rowToEdit){
    event.preventDefault();
    if(!validateInputs())
        return;

    table.rows[rowToEdit].cells[1].innerHTML = group.value;
    table.rows[rowToEdit].cells[2].innerHTML = name.value;
    table.rows[rowToEdit].cells[3].innerHTML = gender.value;
    table.rows[rowToEdit].cells[4].innerHTML = birthday.value;
    table.rows[rowToEdit].cells[5].innerHTML = status.value;
    hiddenID.value = rowToEdit;

    var editedStudentValues = {
        group: group.value,
        name: name.value,
        gender: gender.value,
        birthday: birthday.value,
        status: status.value,
        ID: hiddenID.value
    }
    console.log(editedStudentValues);

    resetValueById("group");
    resetValueById("name");
    resetValueById("gender");
    resetValueById("birthday");
    resetValueById("status");

    modalAdd.style.display = "none";
}