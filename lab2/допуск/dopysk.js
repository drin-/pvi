const animalAge = document.getElementById("animalAge");
const animalSelect = document.getElementById("animal");
const ageVal = document.getElementById("value");
const ageType = document.getElementById("ageType");
const submitButton = document.getElementById("submit");
const checkbox = document.getElementById("checkbox");
const date = document.getElementById("date");
const loginForm = document.getElementById("form");
const modalDel = document.getElementById("AreYouSureModal");
const yesBut = document.getElementById("yesBut");
const noBut = document.getElementById("noBut");

animalAge.addEventListener("input", (event) =>{
    ageVal.textContent = event.target.value;
})

animalSelect.addEventListener("input", (event) =>{
    if(event.target.value === "Humster"){
        animalAge.max = 30;
        ageType.textContent = " months";
    }
    else if(event.target.value === "Dog"){
        animalAge.max = 30;
        ageType.textContent = " years";
    }
    else if(event.target.value === "Snake"){
        animalAge.max = 5;
        ageType.textContent = " years";
    }
    else if(event.target.value === "Cat"){
        animalAge.max = 20;
        ageType.textContent = " years";
    }
})
yesBut.addEventListener ("click", (e) => {
    e.preventDefault();
    alert("sucsess");
    HideModal();
});
noBut.addEventListener ("click", (e) => {
    e.preventDefault();
    HideModal();
});;
function HideModal(){
    modalDel.style.display = "none";
}
loginForm.addEventListener("submit", (e) => {
    e.preventDefault();
  
    if(checkbox.checked)
    {
        alert("sucsess");
    }
    else
    {
        modalDel.style.display = "block";
    }
  });